# php-exercise

**Forkez** ce repo, puis effectuez chacune de ces listes d'exercices en créant à chaque fois un dossier du nom du sujet (ex: pour php-exer$

Dès que vous finissez un exercice, ajoutez-le et commitez-le.

## Concepts-clés

* Loop
* Function
* Array
* ParamUrl
* Form
* GlobalVariable
* Date
